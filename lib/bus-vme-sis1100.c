/**
 * @copyright Copyright (c) 2016 CERN
 * @author: Federico Vaga <federico.vaga@cern.ch>
 * @license: LGPLv3
 */
#include <stdlib.h>
#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <errno.h>
#include <string.h>
#include <sys/ioctl.h>
#include "sis1100_var.h"

#include "ual-int.h"


struct ual_bar_vme {
        int fd_rem, fd_ctrl;
        void *map;
        unsigned used;
};

static int ual_vme_map(struct ual_bar *bar)
{
	struct ual_desc_vme *vme = &bar->desc.vme;
	struct ual_bar_vme *bvme;
        u_int32_t desc[4];
        struct sis1100_ctrl_reg reg;
        unsigned i;

	bvme = bar->bus_data;
	if (!bvme) {
		errno = UAL_ERR_NOT_OPEN;
		return -1;
	}
	if (bvme->used) {
		errno = UAL_ERR_ALREADY_MAPPED;
		return -1;
	}

	if (vme->size & (getpagesize()-1)
            || vme->size > (4 << 20)) {
		errno = UAL_ERR_INVALID_SIZE;
		return -1;
	}

	if (vme->offset & (getpagesize()-1)) {
		errno = UAL_ERR_INVALID_OFFSET;
		return -1;
	}

        desc[0] = 0xff010800;
        desc[1] = vme->am;
        desc[2] = vme->offset & 0xffc00000; /* Windows of 4MB */
        desc[3] = 0;

        /* Set window.  */
        reg.offset=0x400;
        for (i=0; i<4; i++) {
                reg.val=desc[i];
                ioctl(bvme->fd_ctrl, SIS1100_CTRL_WRITE, &reg);
                reg.offset+=4;
        }

	/* Do mmap */
	bar->ptr = bvme->map + (vme->offset & 0x3fffff);

        bvme->used = 1;
	return 0;

}

static int ual_vme_unmap(struct ual_bar *bar)
{
	struct ual_bar_vme *bvme = bar->bus_data;

	if (!bvme) {
		errno = UAL_ERR_NOT_OPEN;
		return -1;
	}

	if (!bvme->used) {
		errno = UAL_ERR_NOT_MAPPED;
		return -1;
	}

        bvme->used = 0;

	return 0;
}


int ual_vme_open(struct ual_bar *bar)
{
	struct ual_bar_vme *bvme;

	bvme = malloc(sizeof(struct ual_bar_vme));
	if (!bvme)
		return -1;

        bvme->fd_rem = open("/dev/sis1100_00remote", O_RDWR, 0);
        bvme->fd_ctrl = open("/dev/sis1100_00ctrl", O_RDWR, 0);
        if (bvme->fd_rem < 0 || bvme->fd_ctrl < 0) {
                close(bvme->fd_rem);
                free (bvme);
                return -1;
        }

        bvme->used = 0;

        /* Map 16MB of vme space.  */
        bvme->map = mmap(0, 0x1000000, PROT_READ|PROT_WRITE, MAP_SHARED, bvme->fd_rem, 0);

        if (bvme->map == MAP_FAILED) {
                close (bvme->fd_rem);
                close (bvme->fd_ctrl);
                free(bvme);
                return -1;
        }

	bar->bus_data = bvme;

        return 0;
}

int ual_vme_close(struct ual_bar *bar)
{
	struct ual_bar_vme *bvme = bar->bus_data;

	if (!bvme) {
		errno = UAL_ERR_NOT_OPEN;
		return -1;
	}

        close (bvme->fd_rem);
        close (bvme->fd_ctrl);
        munmap(bvme->map, 16 << 20);

	free(bvme);
	bar->bus_data = NULL;

	return 0;
}



static struct ual_bus_operations ual_vme_op = {
	.open = ual_vme_open,
	.close = ual_vme_close,
	.map = ual_vme_map,
	.unmap = ual_vme_unmap,
};

struct ual_bus ual_vme = {
	.name = "VME",
	.type = UAL_BUS_VME,
	.op = &ual_vme_op,
};
