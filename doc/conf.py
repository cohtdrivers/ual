# SPDX-FileCopyrightText: 2024 CERN
#
# SPDX-License-Identifier: BSD-3-Clause

extensions = ['breathe']
breathe_projects = {'ual': 'doxy-libual/xml'}
breathe_default_project = 'ual'
html_theme = 'sphinx_rtd_theme'
project = 'Universal Access Library'
copyright = '2024, Federico Vaga'
author = 'Federico Vaga'
templates_path = ['_templates']
exclude_patterns = ['_build', 'Thumbs.db', '.DS_Store', '.venv']
html_theme = 'sphinx_rtd_theme'
html_static_path = ['_static']
